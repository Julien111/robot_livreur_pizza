<?php

/**
 * La classe RobotLivreurPizza décrit le comportement du tout nouveau robot livreur de pizzas de JBHuet.com !
 *
 * Le nouveau robot livreur de pizzas de JBHuet.com est une formidable machine qui va chercher pour vous la pizza qui sort du four :
 * votre pizza préférée vient à vous, sans que vous ayez à bouger.
 * En pleine session de codage intense, pas possible de vous lever pour récupérer la pizza alors que le four sonne ? Pas de souci !
 * Le robot livreur de pizzas de JBHuet.com vous l'apporte sans effort, ou presque.
 * Cette merveilleuse machine se programme très simplement grâce à sa classe qui décrit très précisément tout ce qu'il peut faire pour vous.
 * Il vous suffit d'utiliser cette classe pour décrire à votre robot comment vous apporter votre délicieuse pizza.
 *
 * Ce robot perfectionné est doté d'un écran pour afficher les informations dont vous pourriez avoir besoin.
 * Vous pouvez même lui faire vous souhaiter un bon appétit !
 * NB. : ce robot étant encore à l'état de prototype, l'écran fournit actuellement ne peut afficher que des messages de 255 caractères maximum.
 * Grâce à ses capteurs (testeur de pente dans le sens du déplacement, testeur de distance aux obstacles face à lui, testeur de position de la pizza par rapport au plateau)
 * le robot peut s'adapter à de nombreuses situations.
 *
 * @author Jean-Bernard HUET <contact@jbhuet.com>
 *
 * @version 1.0.0
 *
 * PS. : les robots livreurs de pizzas existent déjà https://www.youtube.com/watch?v=rb0nxQyv7RU et https://www.youtube.com/watch?v=mIwDhnPnb4o
 */

class RobotLivreurPizza 
{
    
    //*Attributs privés

    /** @var string $Message Le message affiché à l'écran. */
    private $messageEcran = "";

    /** @var integer CAPA_AFFICH Constante représentant le nombre maximal de caractères affichés à l'écran */
    private const CAPA_AFFICH = 255;
    /** @var integer HAUTEUR_MAX Constante représentant la hauteur max que le robot peut lever avec son vérin */
    private const HAUTEUR_MAX = 110;
    /** @var integer HAUTEUR_MINI Constante représentant la hauteur mini que le robot va avoir avec son vérin */
    private const HAUTEUR_MINI = 40;
    /**
     * Création d'une classe RobotLivreurPizza
     *
     * Description : cette classe décrit le comportement d'un robot qui livre des pizzas.
     *
     * Renseigner la ligne suivante s'il y a un paramètre. Ajouter autant de ligne d'information que de paramètre de la fonction.
     * @param <string> <$messageEcran> <Description du paramètre : ce paramètre va être utilisé pour afficher les message du robot, plusieurs méthodes vont intéragir avec ce paramètre.>
     */
    public function __construct(string $messageEcran) {
        $this->messageEcran = $messageEcran;
    }
    /**
     * fonction messageDepart()
     * Le robot affiche un message de démarrage.
     */
    public function messageDepart () {
        $this->AfficherMessage("La pizza est prête. Action : aller au four.".PHP_EOL);
    }

    //*Méthode public

    /**
     * fonction Avancer
     * Le robot affiche la distance en mètres qu'il va parcourir pour avancer.
     */
    public function Avancer( float $distance ) {
        $this->AfficherMessage("Avancer de " .$distance. " mètres.".PHP_EOL);
    }

    /**
     * function Reculer
     * Le robot affiche la distance en mètres qu'il va parcourir pour reculer.
     */
    public function Reculer(float $distance) {
        
        $this->AfficherMessage("Reculer de ".$distance." mètres.".PHP_EOL);
    }

    /**
     * fonction TournerDroite 
     * affiche un message quand le robot tourne à droite
     */
    public function TournerDroite(int $degre) {
        //*Le robot affiche le nombre de degrés vers la droite dont il tourne sur lui-même 
        $this->AfficherMessage("Tourner vers la droite de " .$degre. " degrés.".PHP_EOL);
    }

    /**
     * Méthode TournerGauche :
     * Affiche un message quand le robot tourne à gauche.
     */
    public function TournerGauche(int $degre) {
        /* Le robot affiche le nombre de degrés vers la gauche dont il tourne sur lui-même */
        $this->AfficherMessage("Tourner vers la gauche de ".$degre." degrés.".PHP_EOL);
    }

    /**
     * Fonction MonterPlateau :
     * Cette fonction a une première condition, elle vérifie que la hauteur passé en paramètre est comprise entre 40 et 110 cm.
     * Si la hauteur est à 90 cm, hauteur du four alors la pizza peut être tirée , méthode TirerPizzaSurPlateau() et enclenche la méthode Descendre plateau.
     * Si la hauteur est à 80 cm, hauteur du bureau, alors la pizza peut-être poussée PousserPizzaSurPlateau() et la fonction messageFin() est enclenchée.
     * Si la hauteur est en dehors de la plage, alors un message d'erreur s'affiche.
     * La constante HAUTEUR_MAX fixe la hauteur max du tableau à 110 cm.
     */
    public function MonterPlateau(int $hauteur, string $support) {

        if($hauteur >= self::HAUTEUR_MINI && $hauteur <= self::HAUTEUR_MAX){

            
            if($hauteur < 80)
            {
                return  $this->AfficherMessage("Le plateau n'est pas assez haut par rapport au support.".PHP_EOL);
            }
            else if($hauteur == 80 && $support == "bureau")
            {
                $this->AfficherMessage("Le plateau est à la bonne hauteur par rapport au bureau.".PHP_EOL);
                $this->PousserPizzaSurPlateau(80, 1);
                $this->messageFin();
                $this->DescendrePlateau(self::HAUTEUR_MINI);                
            }
            else if($hauteur == 90 && $support == "four")
            {
                $this->AfficherMessage("Le plateau est à la bonne hauteur par rapport au four.".PHP_EOL);
                $this->TirerPizzaSurPlateau(90, 1);
                $this->DescendrePlateau(self::HAUTEUR_MINI);
            }
            else
            {
                return $this->AfficherMessage("Le plateau est trop haut par rapport au support.".PHP_EOL);
            }
        }
        else
        {
            return $this->AfficherMessage("Le plateau est en dehors de la plage.".PHP_EOL);
        }
    
    }

    /**
     * Méthode DescendrePlateau :
     * Si le plateau est à 40 cm de hauteur alors déplacement autorisé.
     * Si le plateau n'est pas à 40cm de hauteur alors le déplacement du robot est interdit.
     */
    public function DescendrePlateau(int $hauteur) {
    
        if($hauteur == 40)
        {
            return $this->AfficherMessage("Le plateau est redescendu à 40 cm. Autorisation de déplacement.".PHP_EOL);
        }
        else
        {
            return $this->AfficherMessage("Le plateau n'est pas à la bonne hauteur. Déplacement interdit.".PHP_EOL);
        }
    }

    /**
     * Quand la pizza est à la bonne hauteur, ici 80 cm et que l'argument spatule est évalué à true.
     * La pizza est poussé dans l'assiette. Message de confirmation.
     * Sinon affiche que la pizza n'a pas été tirée.
     */
    public function PousserPizzaSurPlateau(int $hauteur, int $spatule) {
        
        if($hauteur == 80 && $spatule == 1)
        {
            return $this->AfficherMessage("La pizza a été poussée dans l'assiette avec la spatule.".PHP_EOL);
        }
        else if($hauteur == 80 && $spatule == 0)
        {
            return $this->AfficherMessage("La spatule ne fonctionne pas.".PHP_EOL);
        }
        else
        {
            return $this->AfficherMessage("La pizza n'a pas été poussée. La hauteur du plateau n'est pas bonne".PHP_EOL);
        }
    }

    /**
     * Méthode : TirerPizzaSurPlateau
     *  Ici, si la hauteur est égal à 90 cm (hauteur du four), et que la pince est évalué à true alors la pizza est tirée du four.
     * Si la pince est évalué à false alors affiche le message : "La pince ne fonctionne pas."
     * Sinon affiche un message que la pizza n'a pas été tirée.
     */
    public function TirerPizzaSurPlateau(int $hauteur, int $pince) {
        
        if($hauteur == 90 && $pince == 1)
        {
            return $this->AfficherMessage("La pizza a été tirée du four avec la pince et elle est sur le plateau.".PHP_EOL);
        }
        else if($hauteur == 90 && $pince == 0)
        {
            return $this->AfficherMessage("La pince ne fonctionne pas.".PHP_EOL);
        }
        else
        {
            return $this->AfficherMessage("La pizza n'a pas été tirée. Le plateau n'est pas à la bonne hauteur.".PHP_EOL);
        }
    }

    /**
     * Méthode messageFin :
     * Le robot affiche un message de fin quand la pizza est dans l'assiette.
     */
    public function messageFin () {
        $this->AfficherMessage("Votre pizza est arrivée. Bon appétit.".PHP_EOL);
    }

    /**
     * Cette fonction affiche des tâches qu'accomplient le robot
     * Cette méthode appelle un autre fonction qui est TesterLongueurMessage
     */
    public function AfficherMessage(string $message) {
        if($this->messageEcran = $this->TesterLongueurMessage($message))
        {
        printf($this->messageEcran);
        }
        else{
            return "Erreur message.";
        }
    }

    /**
     * Vérifie que le message à afficher ne dépasse pas la capacité de l'écran
     *
     * La méthode privée TesterLongueurMessage teste si le message passé en paramètre dépasse la capacité d'affichage de l'écran.
     * Si le message dépasse la capacité de l'écran, tous les caractères au-delà de la capacité maximale de l'écran sont supprimés
     *    et "..." est ajouté à la fin du nouveau message.
     * NB. : la longueur maximale du nouveau message ("..." compris) ne peut pas dépasser la capacité d'affichage de l'écran.
     *La fonction substr(), coupe une chaîne de caractères selon la taille qu'on lui définit.
     * @link https://www.php.net/manual/fr/language.oop5.constants.php Pour comprendre la notation self::CAPA_AFFICH
     * @link https://www.php.net/manual/fr/function.strlen.php Pour savoir ce que fait la fonction PHP strlen
     *
     * @param string $MessagePossible Valeur du message avant réduction de la longueur si celle-ci dépasse la capacité de l'écran
     * @return string Valeur du message après réduction éventuelle de la longueur
     */
    private function TesterLongueurMessage( string $MessagePossible ): string {

        //* var_dump(strlen($MessagePossible));

        if(strlen($MessagePossible) > self::CAPA_AFFICH) {  
            return substr($MessagePossible, 0 , 251)." ...";
        }
        else{
        return $MessagePossible;
        }
    }


}

/* Ecrivez ci-dessous le code qui sera transmis à votre robot pour aller chercher votre pizza dans le four et l'apporter à votre bureau
En imaginant que vous possédez ce robot, et que vous êtes installé·e à votre bureau, programmez le robot pour qu'il vous rapporte votre pizza toute chaude.
Donnez des ordres au robot en fonction de la réalité de votre logement.
Le robot peut partir de n'importe quel point (sous votre bureau, un placard, un coin de votre cuisine).
On considère que la porte du four est ouverte, et qu'elle n'empêche pas le robot d'atteindre la pizza.
Le robot doit déposer la pizza dans une assiette posée au bord de votre bureau.
La dernière instruction que le robot devra exécuter est de vous souhaiter un bon appétit.*/

$robot = new RobotLivreurPizza("");

$four = "four";
$bureau = "bureau";


//* Le robot est placé à côté de l'entrée

$robot->messageDepart();
$robot->Avancer(5);
$robot->TournerGauche(90);
$robot->Avancer(1.1);

//* Le robot est devant le four

$robot->MonterPlateau(40, $four);
$robot->MonterPlateau(70, $four);
$robot->MonterPlateau(90, $four);

//* Pizza récupérée
//* Le robot va direction le bureau

$robot->Reculer(1.1);
$robot->TournerGauche(90);
$robot->Avancer(2);
$robot->TournerGauche(90);
$robot->Avancer(4);
$robot->TournerDroite(90);
$robot->Avancer(2.5);

//* Le robot est devant le bureau, il va devant déposer la pizza.

$robot->MonterPlateau(40, $bureau);
$robot->MonterPlateau(65, $bureau);
$robot->MonterPlateau(80, $bureau);

//* La pizza est arrivée dans l'assiette. Et la fonction messageFin() affiche le message "bon appétit"
//* Voir le terminal avec l'instruction "php RobotLivreurPizza.php".




